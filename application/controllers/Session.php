<?php
/**
 * Controlador Begin para manejar el CRUD de la 
 * aplicación de usuario con CodeIgniter
 * 
 * @author Juan Sebastian <innovasistemas@gmail.com>
 * @abstract Proyecto para gestionar usuarios
 * @copyright (c) 201/, Jaime Montoya
 * @creation-date 20/11/2018
 * @update-date 21/11/2018
 * @version 1.0
 * @class Session.controlador para las sessiones
 * @link http://localhost/users-codeigniter/index.php/session/read
*/
defined('BASEPATH') OR exit('No direct script access allowed');

class Session extends CI_Controller 
{

    /**
     * Controlador Session para manejar el inicio de sesión con CodeIgniter y controlar los accesos a las distintas páginas
     */

    private $arrayData;


    public function __construct()
    {
        parent::__construct();
        $this->load->model('ModelDatabase');
    }


    public function __destruct()
    {

    }


    public function index()
    {
        $this->load->view('login', $this->arrayData);
    }


    public function validateSession()
    {
        $this->form_validation->set_rules('user', 'Usuario', 'trim|required');
        $this->form_validation->set_rules('password', 'Contraseña', 'trim|required');

        if($this->form_validation->run() == FALSE){
            $this->index();
        }else{
            $arrayDataForm = [
                'field' => $this->input->post('enterUser'),
                'data' => $this->input->post('user'),
                'password' => $this->input->post('password')
            ];

            $arrayResult = $this->ModelDatabase->validateLogin($arrayDataForm);

            if($arrayResult->num_rows() == 0){
                $this->arrayData['messageConnection'] = "Usuario o contraseña inválidos";
                $this->index();
            }else{
                $this->arrayData['messageConnection'] = "Acceso exitoso. Bienvenido a la aplicación";
                //$this->session->set_userdata($arrayDataForm);
                //$this->arrayData['userName'] = $this->session->userdata['data'];
                $this->arrayData['userName'] = $this->input->post('user');
                foreach ($arrayResult->result() as $row) {
                    $this->arrayData['fullNameUser'] = $row->nombre; 
                }

                include "Begin.php";
                $objBegin = new Begin();
                $objBegin->setArrayDataSession('userName', $this->arrayData['userName']);
                $objBegin->setArrayDataSession('fullNameUser', $this->arrayData['fullNameUser']);
                $objBegin->read();
                
            }
            
        }
    }


    public function closeSession()
    {
        $this->session->sess_destroy();
        $this->arrayData['closeSession'] = "Sesión finalizada";
        $this->index();
    }


}